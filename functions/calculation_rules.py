"""
File consists of several functions for the calculation rules of FAIR Quality KPIs
"""


def test_function():
    """Test function to check module functionality"""
    print("You called the test function.")


def kpi_sum(*args):
    """
    Calculates the sum of one or more integers or lists.

    Args:
        *args: One or more integers or lists.

    Returns:
        total (int): The sum of all given integers and/or the sum
        of all items in all given lists.

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than int or list).
    """
    total = 0
    for arg in args:
        if isinstance(arg, int):  # Check if argument is an integer
            total += arg
        elif isinstance(arg, list):  # Check if argument is a list
            total += sum(arg)
        else:
            raise TypeError(
                f"Unsupported type {type(arg)} passed to kpi_sum()")
    return total

    # if arguments are handed over not as a list: sum(list(args))


# Add new functions for calculating some real complicated metrics
#Aufwand - Gesamtkosten des Fahrzeugs

combined_costs = 0
for c in car.get_component_list(-1):
    combined_costs += c.properties["price [Euro]"]


#Verfügbarkeit - Lieferzeit des Autos bzw. des Teils, das die längste Lieferzeit hat 
delivery_time = 0
for c in car.get_component_list(-1):
    if delivery_time < c.properties["delivery time [days]"]:
        delivery_time = c.properties["delivery time [days]"]




#Akzeptanz - Gesamtgewicht des Fahrzeugs
combined_weight = 0
for c in car.get_component_list(-1):
    combined_weight += c.properties["mass [g]"]


if __name__ == "__main__":
    """
    Function to inform that functions in this module is
    intended for import not usage as script
    """
    print(
        "This script contains functions for calculating the FAIR Quality KPIs."
        "It is not to be executed independently."
    )
    
    
    
